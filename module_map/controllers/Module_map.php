<?php
class Module_map extends Trongate {

    function index () {

        $dir = APPPATH."modules/";
        $data['modules_dir'] =$dir;
        $data['modules'] = array_diff(scandir($dir), array(".", ".."));
        
        $data['view_module'] = 'module_map';
        $this->view('module_map_view', $data);
        /* Uncomment the lines below, 
         * Change the template method name, 
         * Remove lines above, if you want to load to the template
         */
        //$data['view_module'] = 'module_map';
        //$data['view_file'] = 'module_map_view';
        //$this->template('template method here', $data);
    }

    function _get_view_files($module){
        $search = APPPATH."modules/".$module;
        $views = array_diff(scandir($search.'/views'), array(".", ".."));
        if(count($views )> 0){
            $dir =  '<div class="module-sub-dir"><h3>Views:</h3>';
             foreach($views as $view){
                 $dir .= '<p class="file-names">'.$view.'</p>';
             }
             $dir .= '</div>';
        }
        return $dir;
    }


    function _get_controller_file_methods($module){
        $search = APPPATH."modules/".$module;
                                   
        $controllers = array_diff(scandir($search.'/controllers'), array(".", ".."));
        $dir = '<div class="module-sub-dir"><h3>Controllers:</h3>';
        foreach($controllers as $controller){
             $dir .= '<p class="file-names">'.$controller.'</p><div class="methods">';
             $dir .= '<h4>Functions() </h4><ul>';
                # The Regular Expression for Function Declarations
                $functionFinder = '/function[\s\n]+(\S+)[\s\n]*\(/';
                # Init an Array to hold the Function Names
                $functionArray = array();
                # Load the Content of the PHP File
                $file =  APPPATH.'modules/'.$module.'/controllers/'.$controller;
                                
                $fileContents = file_get_contents( $file );

                # Apply the Regular Expression to the PHP File Contents
                preg_match_all( $functionFinder , $fileContents , $functionArray );

                # If we have a Result, Tidy It Up
                if( count( $functionArray )>1 ){
                    # Grab Element 1, as it has the Matches
                    $functionArray = $functionArray[1];

                    for ($i = 0; $i < count($functionArray); $i++)  {
                        $dir .= '<li>'.$functionArray[$i] ."</li>";
                    }
                }
        }
        $dir .= "</ul></div></div><br>";
        return $dir;
    }

    function _get_assets_files($file_path) {
        $assets = array_diff(scandir($file_path.'/assets'), array(".", ".."));
        if(count($assets )> 0){
            $dir =  '<div class="module-sub-dir"><h3>assets:</h3>';
             foreach($assets as $asset){
              
                    $dir .= '<p class="file-names">'.$asset.'</p>';
                    $files = array_diff(scandir($file_path.'/assets/'.$asset), array(".", ".."));
                    $dir .= '<ul>';
                    foreach($files as $file){
                       $dir .='<li>'.$file.'</li>';
                    }
                    $dir .= '</ul>';
                 
             }
             $dir .= '</div>';
        }
        return $dir;
    }

    function _get_sub_modules($module, $parent_module) {
        $sub_modules = '';
        $view_dir = '';
        $conto_dir = '';
        $assets_dir = '';
               
        $module_path = $parent_module.'/'.$module;
         
        $dir_path = APPPATH."modules/".$parent_module.'/'.$module;
        $module_files = array_diff(scandir($dir_path), array(".", ".."));
        $dir = '';
        foreach($module_files as $module_file){
              
            switch ($module_file) {
                 case "views":
                     $view_dir = $this->_get_view_files($module_path);
                 break;
                 case "controllers":
                     $conto_dir = $this->_get_controller_file_methods($module_path, $module);
                 break;
                 case "assets":
                     $assets_dir = $this->_get_assets_files($module_path);
                 break;
                 default:
                     $sub_modules .= $this->_get_sub_modules($module_path, $module_file, );
             } 
        }
        $dir .=  '<div class="sub-module-container"><h2>'.$module.'</h2>';
        $dir .= '<div class="sub-container">';
        $dir .= $view_dir. $conto_dir.$assets_dir;
                 
       $dir .= '</div></div>';
      
       return $dir; 
    }



    

}