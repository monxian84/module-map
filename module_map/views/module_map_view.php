<h1 class="module-map-headline">Module Map </h1>
<?php
        foreach($modules as $module){
      
               $module_path = $modules_dir.$module;

               $module_files = array_diff(scandir($module_path), array(".", ".."));
               foreach($module_files as $module_file){
                                                
                 switch ($module_file) {
                        case "views":
                        $view_dir = $this->_get_view_files($module);
                        break;
                        case "controllers":
                        $conto_dir = $this->_get_controller_file_methods($module);
                        break;
                        case "assets":
                            $assets_dir = $this->_get_assets_files($module_path);
                        break;
                        default:
                            $sub_modules .= $this->_get_sub_modules($module_file, $module);
                    } 
               }
               echo '<div class="module-container"><h2>'.$module.'</h2>';
               echo '<div class="sub-container">';
               echo $view_dir;
               echo $conto_dir;
               echo $assets_dir;
               echo '</div>';//sub-container 

              if(strlen($sub_modules) > 1){
                echo '<div class="sub-modules"><h2>Sub Modules</h2>'; 
                echo $sub_modules; 
                echo '</div>';
              }
              $sub_modules = " ";
    
                
           echo '</div>';
        }


      

    ?>
<style>
    /* Link to the module_map/assets/css/custom.css */
    @import url('<?= BASE_URL ?>module_map_module/css/custom.css');
  
</style>

<!-- Link to the assets/js/custom.js -->
<script src="<?= BASE_URL ?>module_map_module/js/custom.js"></script>